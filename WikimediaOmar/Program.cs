﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WikimediaOmar
{
    class Program
    {
        static void Main(string[] args)
        {
            TestEfficient();
            Console.ReadKey();            
        }

        static void TestEfficient()
        {
            string fileName = @"C:\Users\kaki\Documents\Visual Studio 2015\Projects\WikimediaOmar\WikiMediaTestDocument.txt";
            string[] fileNames = new string[2];
            fileNames[0] = fileName;
            fileNames[1] = fileName;

            List<Processers> processers = new List<Processers>();
            processers.Add(new Tokenizer());
            processers.Add(new Normalize());

            EfficientFrequencyList freqList = new EfficientFrequencyList();
            var frequenciesOfWords = freqList.GetFrequencyList(fileNames);

            Console.WriteLine("Word\t\t\tFrequencies");
            foreach (var word in frequenciesOfWords.Where(x => x != null).OrderByDescending(x => x.wordFrequncy))
            {
                Console.WriteLine("{0}\t\t\t{1}", word.word, word.wordFrequncy);
            }
        }

        static void Test()
        {
            string fileName = @"C:\Users\kaki\Documents\Visual Studio 2015\Projects\WikimediaOmar\WikiMediaTestDocument.txt";
            string[] fileNames = new string[2];
            fileNames[0] = fileName;
            fileNames[1] = fileName;

            List<Processers> processers = new List<Processers>();
            processers.Add(new Tokenizer());
            processers.Add(new Normalize());

            FrequencyList freqList = new FrequencyList();
            var frequenciesOfWords = freqList.GetFrequencyList(fileNames);

            Console.WriteLine("Word\t\t\tFrequencies");
            foreach(var word in frequenciesOfWords.OrderByDescending(x => x.Value))
            {
                Console.WriteLine("{0}\t\t\t{1}", word.Key, word.Value);
            }            
        }
    }

    class EfficientToken
    {
        public int wordFrequncy;
        public string word;

        public EfficientToken(string word)
        {
            this.word = word;
            this.wordFrequncy = 1;
        }
    }

    class EfficientFrequencyList
    {
        private EfficientToken[] wordFrequency;
        private List<Processers> processers;
        private int totalNumberOfTokens;

        private HashSet<string> uniqueWords;

        public EfficientFrequencyList()
        {
            processers = new List<Processers>();
            processers.Add(new Tokenizer());
            processers.Add(new Normalize());
        }

        public EfficientFrequencyList(List<Processers> processers)
        {
            this.processers = processers;
        }

        //Insteaf of using a Dictionary, use a more Space-Efficient(but slower) data-structure like an array(HashSet which internally uses an array in C#).
        public EfficientToken[] GetFrequencyList(string[] fileNames)
        {            
            string line = String.Empty;
            uniqueWords = new HashSet<string>();

            //Step 1: Read all files to get the total number of tokens
            foreach (var fileName in fileNames)
            {
                System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                while ((line = file.ReadLine()) != null)
                {
                    CountUniqueWords(line);
                }

                file.Close();
            }

            totalNumberOfTokens = uniqueWords.Count();

            //Step 2: Read the files again and this time count the frequncy of each word

            wordFrequency = new EfficientToken[totalNumberOfTokens];
            foreach (var fileName in fileNames)
            {
                System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                while ((line = file.ReadLine()) != null)
                {
                    IncrementFrequencyList(line);
                }

                file.Close();
            }

            return wordFrequency;
        }

        public void CountUniqueWords(string line)
        {
            var words = line.Split(' ');
            foreach (var word in words)
            {
                var currentWord = word;

                foreach (var processer in processers)
                {
                    currentWord = processer.Process(currentWord);

                    if(!uniqueWords.Contains(currentWord))
                    {
                        uniqueWords.Add(currentWord);                        
                    }
                }                
            }
        }

        public void IncrementFrequencyList(string line)
        {
            var words = line.Split(' ');
            foreach (var word in words)
            {
                var currentWord = word;

                foreach (var processer in processers)
                {
                    currentWord = processer.Process(currentWord);                    
                }

                var hashOfCurrentWord = currentWord.GetHashCode();
                var indexOfCurrentWord = hashOfCurrentWord % totalNumberOfTokens;

                if (indexOfCurrentWord < 0)
                    indexOfCurrentWord *= -1;

                if (wordFrequency[indexOfCurrentWord] != null)
                {
                    wordFrequency[indexOfCurrentWord].wordFrequncy++;
                }
                else
                {
                    uniqueWords.Add(currentWord);
                    wordFrequency[indexOfCurrentWord] = new EfficientToken(currentWord);
                }
            }
        }
    }

    class FrequencyList
    {
        private Dictionary<string, int> freqMap;
        private List<Processers> processers;

        public FrequencyList()
        {
            processers = new List<Processers>();
            processers.Add(new Tokenizer());
            processers.Add(new Normalize());
        }

        public FrequencyList(List<Processers> processers)
        {
            this.processers = processers;
        }

        public Dictionary<string, int> GetFrequencyList(string[] fileNames)
        {
            freqMap = new Dictionary<string, int>();
            string line = String.Empty;

            foreach (var fileName in fileNames)
            {
                System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                while ((line = file.ReadLine()) != null)
                {
                    IncrementFrequencyList(line);
                }

                file.Close();
            }

            return freqMap;
        }

        public Dictionary<string, int> GetFrequencyList(string fileName)
        {
            freqMap = new Dictionary<string, int>();
            string line = String.Empty;
            System.IO.StreamReader file =  new System.IO.StreamReader(fileName);
            while ((line = file.ReadLine()) != null)
            {
                IncrementFrequencyList(line);
            }

            file.Close();

            return freqMap;
        }

        public void IncrementFrequencyList(string line)
        {
            var words = line.Split(' ');
            foreach (var word in words)
            {
                var currentWord = word;

                foreach (var processer in processers)
                {
                    currentWord = processer.Process(currentWord);
                }
                
                if (freqMap.ContainsKey(currentWord))
                {
                    freqMap[currentWord]++;
                }
                else
                {
                    freqMap.Add(currentWord, 1);
                }
            }
        }
    }

    public abstract class Processers
    {
        public abstract string Process(string word);
        public abstract string Process(string word, string option);
    }

    class Normalize : Processers
    {
        public override string Process(string word)
        {
            return word.ToLower();
        }

        public override string Process(string word, string option)
        {
            if (option == null)
                return word.ToLower();
            else if (option == "toLower")
                return word.ToLower();
            else if (option == "toUpper")
                return word.ToUpper();
            else
                return word.ToLower();
        }
    }

    class Tokenizer : Processers
    {
        private static Regex r;

        public Tokenizer() {
            r = new Regex("[a-zA-Z]");
        }

        public override string Process(string word)
        {
            var newWord = new StringBuilder();
            for(int i = 0; i< word.Length;i++)
            {
                if(r.IsMatch(word[i].ToString()))
                {
                    newWord.Append(word[i]);
                }
            }

            return newWord.ToString();
        }

        public override string Process(string word, string option)
        {
            var newWord = new StringBuilder();
            for (int i = 0; i < word.Length; i++)
            {
                if (r.IsMatch(word[i].ToString()))
                {
                    newWord.Append(word[i]);
                }
            }

            return newWord.ToString();
        }


    }
}
